#include "config.h"

/*
 * Common handler prototype:
 *	- inline parser for bold/italic, inline escape and inline code
 *	- block parsers:
 *		o headings
 *		o lists (can be split in two functions, but not recommended)
 *		o code
 *		o default (paragraph)
 */

/*
 * Common parsers prototype :
 * start and end: const pointers to the start and end of parsing target
 * returns, as an offset to start, where we should continue parsing
 * (which is also the number of character parsed)
 */

#ifdef CONFIG_HTML
int html_parse_inline(const char* start, const char* end, unsigned int closeall);
int html_block_heading(const char* start, const char* end);
int html_block_default(const char* start, const char* end);
int html_block_lists(const char* start, const char* end);
int html_block_code(const char* start, const char* end);

#include "html_handler.h"
#endif

#ifdef CONFIG_LATEX
int latex_parse_inline(const char* start, const char* end, unsigned int closeall);
int latex_block_heading(const char* start, const char* end);
int latex_block_default(const char* start, const char* end);
int latex_block_lists(const char* start, const char* end);
int latex_block_code(const char* start, const char* end);

#include "latex_handler.h"
#endif

/*
 * Data structures:
 *	- target_tag contains the option that match the language
 *		THE MATCH BETWEEN THAT ARRAY AND THE OTHERS ARE BASED ON LINE NUMBER,
 *		SO BE CAREFUL OF POSITION !!
 *	- anchor contains markick language element (actually block types). Their position match the handler.
 *	- handler contains all the handlers, one line per language
 *	- target_header/footer: the string to add to enclose the parsed markick in a valid document
 */

char* anchor[5] = {"", "```", "#", "-", "."};

int (*handler[][5])(const char* start, const char* end) = {
#ifdef CONFIG_HTML
	{html_block_default, html_block_code, html_block_heading, html_block_lists, html_block_lists},
#endif
#ifdef CONFIG_LATEX
	{latex_block_default, latex_block_code, latex_block_heading, latex_block_lists, latex_block_lists},
#endif
};

char* target_tag[] = {
#ifdef CONFIG_HTML
	"-html",
#endif
#ifdef CONFIG_LATEX
	"-latex",
#endif
};

char* target_header[] = {
#ifdef CONFIG_HTML
	HTML_HEADER,
#endif
#ifdef CONFIG_LATEX
	LATEX_HEADER,
#endif
};

char* target_footer[] = {
#ifdef CONFIG_HTML
	HTML_FOOTER,
#endif
#ifdef CONFIG_LATEX
	LATEX_FOOTER,
#endif
};
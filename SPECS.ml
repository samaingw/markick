# Notes

As expected, these specs are written in full and pure markick (no arbitrary code included).
It can be rendered in whatever format you wish.

# Definition

TODO

# Markup

## Blocks

Available blocks are:

- standard (paragraph) blocks
- list blocks
- heading blocks
- code blocks
- escape blocks

**Blocks must be separated with at least one empty line.**

### Escaping

Escaping is done with a backslash (`\`). All the characters after it are escaped until another backslash is found.
Backslash escaping is done with empty zones (so `\something\`).
This escaping is inline (but can be declared inside a new block).

The syntax `\\\` is provided to make block escaping (and allow inner `\`, of course).
It must be provided at the beginning of a line.
If it is followed by something on the same line, that something gets escaped.

### Headings

Headings are declared with # at the beginning of a line, followed by the heading text.
The number of # determine the level of the heading.
*One or no* space is needed between heading level and heading text.
Everything after the heading declaration get copied *in particular,
`#  something` will render with one space*.
Moreover, unlike markdown, **trailing # after the heading text will be copied**.

**The setext syntax isn't supported**.

### Code

Code declaration works like escaping, just replace `\` with \`\.
**Tab/space syntax isn't supported**. Make a \```\ block if you want lines of code.

### Lists

**Bullet lists are declared with dashes at the beginning of a line, and dashes only**.
**Unlike markdown, nested level works like headings**. Example:

- This is bullet 1 on level 1
-- This is bullet 1 on level 2
-- This is bullet 2 on level 2
- This is bullet 2 on level 2

Enumerated lists are declared with `.`. Nesting is done like bullet list.

. Point 1 level 1
.. Point 1 level 2
. Point 2 level 1

Bullet list and enumerated list items can have a space between declaration (`-` or `.`) and text,
but like headings this is not mandatory. Item text can split in several lines,
but the exact behaviour (in particular, adding a space or not) is unspecified.

Nesting a n+1 level enumerated list in a n level bullet list will work as expected.
Nesting a n+1 bullet list inside  an n level enumerated list too.

**Every over mix of enumerated list and bullet list is *unspecified*, as well as
trying to make a n+2 level list from a n level list (because this is weird) **.

## Inline

### Bold/Italic

Works like markdown: \*\ for *italic*, \**\ for **bold**, \***\ for ***both***.
Note that \** *This will* render**\ ** *as* expected**, but `***` is **unspecified**.
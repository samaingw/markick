/* markick - simple markup
 * Copyright (C) <2017, ...> Gwenaël Samain
 *               <2007, 2008> Enno Boland <g s01 de>
 *
 * See LICENSE for further informations
 * Handler structure is exposed in markick.h
 * config.h is left to user, so very simple
 * main manages options and enclosing in a valid document
 * process do the main parsing loop
 */

#include "markick.h"

#define LENGTH(x)  sizeof(x)/sizeof(x[0])
#define ADDC(b,i)  if(i % BUFSIZ == 0) { b = realloc(b, (i + BUFSIZ) * sizeof(char)); if(!b) eprint("Malloc failed."); } b[i]
#define VERSION "a01"

/*int parse_target;*/

/* Utilities */
void eprint(const char *format, ...);
static void *ereallocz(void *p, size_t size);
/* --------- */
static void process(const char *begin, const char *end, int parse_target);     /* Processes range between begin and end. */


/*
Special parser: common to every handler (just raw copy)
Name: escape
Arguments:
	- start: starting adress in the character buffer
	- end: last adress in the character buffer
Returns the number of parsed characters
*/
int escape(const char* start, const char* end);

int escape(const char* start, const char* end)
{
	unsigned int i;
	for (i = *(start+3) == '\n' ? 4 : 3;
	strncmp(start+i, "\\\\\\", 3); ++i)
		fputc(*(start+i), stdout);
return i+5;
}


inline void
eprint(const char *format, ...) {
	va_list ap;

	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

void *
ereallocz(void *p, size_t size) {
	void *res;
	if(p)
		res = realloc(p , size);
	else
		res = calloc(1, size);

	if(!res)
		eprint("fatal: could not malloc() %u bytes\n", size);
	return res;
}

/*
process - do the main parsing loop (and gives handler a fresh block)
Arguments:
	- start: starting adress in the character buffer
	- end: last adress in the character buffer
Returns nothing, as the file will be completely eaten
*/
void
process(const char *begin, const char *end, int parse_target) {
	const char *p;
	int done;
	unsigned int i;

	for(p = begin; p < end;) {
		while(*p == '\n')
			if(++p == end)
				return;
		done = 0;
		for (i = 1; i < LENGTH(anchor); ++i)
			if (!strncmp(p, anchor[i], strlen(anchor[i])))
				done += handler[parse_target][i](p, end);
		if (!done) {
			if (!strncmp(p, "\\\\\\", 3))
				done += escape(p, end);
			else
				done += handler[parse_target][0](p, end);
		}
		p += done;
	}
}

int
main(int argc, char *argv[]) {
	char *buffer = NULL;
	int s, i, target_match, parse_target, enclose = 0;
	unsigned long len, bsize;
	FILE *source = stdin;
	parse_target = 0;

	for (i = 1; i < argc; i++)
	{
		if(!strcmp("-v", argv[i]))
			eprint("Markick %s (C) Gwenaël Samain, Enno Boland\n",VERSION);
		else if (argv[i][0] != '-')
			break;
		else
		{
			if (!strcmp("--", argv[i]))
			{
				i++;
				break;
			}
			else if (!strcmp("-e", argv[i]))
				enclose = 1;
			else
			{
				target_match = 0;
				for (s = 0; s < LENGTH(target_tag); ++s)
					if (!strcmp(target_tag[s], argv[i]))
					{
						parse_target = s;
						++target_match;
					}
				if (!target_match)
					eprint("Usage %s [options] [file]\n -html parse to html\n -latex parse to LaTeX\n -e enclose document\n", argv[0]);
			}
		}			
	}
	if(i < argc && !(source = fopen(argv[i], "r")))
		eprint("Cannot open file `%s`\n",argv[i]);
	bsize = 2 * BUFSIZ;
	buffer = ereallocz(buffer, bsize);
	len = 0;
	while((s = fread(buffer + len, 1, BUFSIZ, source))) {
		len += s;
		if(BUFSIZ + len + 1 > bsize) {
			bsize += BUFSIZ;
			if(!(buffer = realloc(buffer, bsize)))
				eprint("realloc failed.");
		}
	}
	buffer[len] = '\0';

/* Parsing starts here */
	fputs(enclose ? target_header[parse_target]: "", stdout);
	process(buffer, buffer + len, parse_target);
	fputs(enclose ? target_footer[parse_target]: "", stdout);
/* Parsing ends here */

	fclose(source);
	free(buffer);
	return EXIT_SUCCESS;
}

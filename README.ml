# Markick, a markdown son

## Why markick ?

### Markdown is great

- Markdown can be read as is
- Markdown is straightforward to write
- Markdown is parsable quite easely \\\ blep\\\

### But

- Markdown has several dialects (common markdown attempts to fix this, but too late :( ),
	which makes it quite difficult to know when your source will successfully parse or not
- Markdown is messy on syntax (allow overlapping characters, 2 VERY different headings syntax),
	which makes it unncessary complicated to parse
- Markdown is perfect for HTML, but any other exports rely on personnal implementation.
	In particular, YOU CANNOT INSERT ARBITRARY MARKUP CODE (for example LaTeX code).

### Markick solution

- Mimics Markdown (common markdown specs in particular) mostly
- Rigidify syntax to provide lightspeed parser (and clear syntax too, so that you can rely on it)
- Provide the small things (more programmer-ish than forumer-ish) that lacks in Markdown

***This project is still unstable***
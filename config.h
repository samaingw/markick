#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define CONFIG_HTML 1
#define CONFIG_LATEX 1


#define HTML_HEADER "<html>\n"
#define HTML_FOOTER "</html>\n"
#define LATEX_HEADER "\\documentclass{article}\n\\begin{document}\n"
#define LATEX_FOOTER "\\end{document}\n"

#define MAX_NESTED 8

CC = gcc
COMPFLAGS = -Wall -Werror -Os
HANDLERS = *_handler.h

all: markick.o
	$(CC) ./markick.o -o markick

markick.o: markick.c markick.h $(HANDLERS)
	$(CC) $(COMPFLAGS) -c ./markick.c

clean:
	rm ./*.o

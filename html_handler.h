int html_parse_inline(const char* start, const char* end, unsigned int closeall)
{
	unsigned int i, inline_level = 0;
	static int old_level = 0; /* why static ? because bold/italic can be on multiple lines */
	if (closeall)
	{
		if (old_level & 2)
			fputs("</strong>", stdout);
		if (old_level & 1)
			fputs("</em>", stdout);
		old_level = 0;
		i = 0;
	}
	else
	{
	/* Avoid first blank character, but copies all the rest */
		for (
		i = (*start == ' ' || *start == '	') ? 1 : 0;
		((start+i) < end) && *(start+i) != '\n';
		++i)
		{
			if (*(start+i) == '\\') { /* raw copy until \ */
				for (++i; *(start+i) != '\\';
				fputc( *(start+ (i++)) , stdout));
				/* Case of \\: make a single \ */
				if (*(start+i-1) == '\\')
					fputc('\\', stdout);
			}
			else if (*(start+i) == '`') {
				fputs("<code>", stdout);
				for (++i; *(start+i) != '`';
				fputc( *(start+ (i++)) , stdout));
				fputs("</code>", stdout);
			}
			else
			{
				if (*(start+i) == '*')
				{
					for (inline_level = 1; *(start+i+inline_level) == '*'; ++inline_level);
					/* If its bold, there is **, if italics, * and *** for both.
					Consequently, whe can test independently first and second bit to
					see if there is such a flag */
					if ((inline_level & old_level) & 1) /* need to close italic: already present !*/
						fputs("</em>", stdout);
					else if (inline_level & 1)
						fputs("<em>", stdout);
					if ((inline_level & old_level) & 2) /* need to close bold */
						fputs("</strong>", stdout);
					else if (inline_level & 2)
						fputs("<strong>", stdout);
					i += inline_level-1;
					old_level ^= inline_level;
				}
				else
					fputc( *(start+i) , stdout);
			}
		}
	}

return i;
}

int html_block_default(const char* start, const char* end)
{
	unsigned int i;

	fputs("<p>", stdout);

	for (i = 0; (start+i) < end && *(start+i) != '\n'; ++i)
		i += html_parse_inline(start+i, end, 0);/* so html_parse_inline stops to newline, and we skip it with ++i in th for */
	html_parse_inline(0, 0, 1);
	fputs("</p>\n", stdout);

return i+1;
}

int html_block_code(const char* start, const char* end)
{
	unsigned int i;

	fputs("<pre><code>\n", stdout);

	for (i = *(start+3) == '\n' ? 4 : 3;
	strncmp(start+i, "```", 3); ++i)
		fputc(*(start+i), stdout);

	fputs("</code></pre>\n", stdout);

return i+5;
}

int html_block_heading(const char* start, const char* end)
{
	unsigned int nested_lvl;
	unsigned int i;

	for (nested_lvl = 0; *(start+nested_lvl) == '#'; ++nested_lvl);

	fputs("<h", stdout); fputc(nested_lvl + 48, stdout); fputc('>',stdout);

	i = nested_lvl + html_parse_inline(start + nested_lvl, end, 0);
	html_parse_inline(0, 0, 1);
	fputs("</h", stdout); fputc(nested_lvl + 48, stdout); fputs(">\n", stdout);

return i+2;
}

int html_block_lists(const char* start, const char* end)
{
	unsigned int current_nested_level;
	unsigned int new_nested_level;
	short old_type[MAX_NESTED];
	short type;
	unsigned int i, j;

	new_nested_level = 0;
	old_type[0] = 0; /* sentinel */

	for (i = 0; (start+i) < end && *(start+i) != '\n';++i)
	{
		current_nested_level = new_nested_level;
		type = 0;
		new_nested_level = 0;
		if (*(start+i) == '-')
		{
			type = 1;
			for (; *(start+i+new_nested_level) == '-' && (start+i+new_nested_level) != end && new_nested_level < MAX_NESTED; ++new_nested_level);
		}
		else if (*(start+i) == '.')
		{
			type = -1;
			for (; *(start+i+new_nested_level) == '.' && (start+i+new_nested_level) != end && new_nested_level < MAX_NESTED; ++new_nested_level);
		}

		if (type)
		{
			/* 1° Close preceding list-point */
			if (current_nested_level)
				fputs("</li>\n", stdout);
			/* 2° manage nested level */
			if (old_type[0])
			{
				/* Set the underlying list-type for all lower levels */
				for (j = current_nested_level; j < new_nested_level; old_type[j++] = type)
					fputs(type == 1 ? "<ul>\n" : "<ol>\n", stdout);/* TODO see optimization */
				/* Close all upper-level lists */
				for (j = current_nested_level-1; j >= new_nested_level; --j)
					fputs(old_type[j] == 1 ? "</ul>\n" : "</ol>\n", stdout);/* TODO see optimization */
			}
			else
			{
				/* Set the underlying list-type for all lower levels */
				for (j = 0; j < new_nested_level; old_type[j++] = type)
					fputs(type == 1 ? "<ul>\n" : "<ol>\n", stdout);/* TODO see optimization */
			}
		}
		i += new_nested_level;
		if (type)
		{
			for (j = 0; j < new_nested_level; ++j)
				fputc('	', stdout);
			fputs("<li>", stdout);
		}
		i += html_parse_inline(start+i, end, 0);
	}
	fputs("</li>\n", stdout);
	
	html_parse_inline(0, 0, 1); /* TODO: CLOSE BEFORE !!*/
	/* To close the lists, we need to know the latest level, which is in new */
	for (j = new_nested_level; j > 0; --j)
		fputs(old_type[j-1] == 1 ? "</ul>\n" : "</ol>\n", stdout);/* TODO see optimization */

return i+1;
}